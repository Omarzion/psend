extern crate log;
#[macro_use]
extern crate serde_derive;
extern crate unwrap;
extern crate clap;
extern crate crust;
extern crate maidsafe_utilities;
extern crate rand;
extern crate serde_json;


use clap::{App, AppSettings, Arg, SubCommand};

use crust::{Config, ConnectionInfoResult, Uid};
use rand::{Rand, Rng};
use std::cmp;
use std::collections::{BTreeMap, HashMap};
use std::io;
use std::str::FromStr;
use std::sync::mpsc::{channel, RecvTimeoutError, Sender};
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::{Duration, Instant};
use maidsafe_utilities::event_sender::{ MaidSafeObserver, MaidSafeEventCategory };

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq, Ord, PartialOrd, Hash)]
struct UniqueId([u8; 20]);
impl Uid for UniqueId {}
impl Rand for UniqueId {
  fn rand<R: Rng>(rng: &mut R) -> Self {
    let mut inner = [0; 20];
    rng.fill_bytes(&mut inner);
    UniqueId(inner)
  }
}

type PrivConnectionInfo = crust::PrivConnectionInfo<UniqueId>;
type Service = crust::Service<UniqueId>;

fn main() {

  // Construct Service and start listening
  let (channel_tx, channel_rx) = channel();
  let (category_tx, category_rx) = channel();
  // let (exit_tx, exit_rx) = channel();



  let mut config = Config::default();
  let crust_event_category = MaidSafeEventCategory::Crust;
  let event_sender = MaidSafeObserver::new(channel_tx, crust_event_category, category_tx);

  let mut service = Service::with_config(event_sender, config, rand::random());
  

  let matches = App::new("psend")
    .about("send or recieve a file with a link using p2p")
    .version("0.0.1")
    .arg(
      Arg::with_name("recieve-string")
        .long("recieve")
        .short("r")
        .value_name("CONNECTION STRING")
        .help("recieve a file by a peer by providig a connection string")
    ).arg(
      Arg::with_name("send-filename")
        .long("send")
        .short("s")
        .value_name("FILE")
        .help("request a share string for a file (default)")
    ).get_matches();


}
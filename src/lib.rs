#[macro_use]
extern crate serde_derive;
extern crate maidsafe_utilities;
extern crate crust;
extern crate rand;

use std::sync::{ mpsc, Arc, Mutex };
use rand::{Rand, Rng};
use maidsafe_utilities::event_sender::{ EventSender, MaidSafeObserver, MaidSafeEventCategory };
// use crust;

// mod network;

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq, Ord, PartialOrd, Hash)]
struct UniqueId([u8; 20]);
impl crust::Uid for UniqueId {}
impl Rand for UniqueId {
    fn rand<R: Rng>(rng: &mut R) -> Self {
        let mut inner = [0; 20];
        rng.fill_bytes(&mut inner);
        UniqueId(inner)
    }
}

pub struct NetworkService {
    channel_rx: mpsc::Receiver<crust::Event<UniqueId>>,
    category_rx: mpsc::Receiver<MaidSafeEventCategory>,
    // service: crust::Service<UniqueId>
    service: Arc<Mutex<crust::Service<UniqueId>>>
}

impl NetworkService {
    pub fn new() -> Self {
        let (channel_tx, channel_rx) = mpsc::channel();
        let (category_tx, category_rx) = mpsc::channel();

        let mut config = crust::Config::default();
        let event_sender = MaidSafeObserver::new(channel_tx, MaidSafeEventCategory::Crust, category_tx);
        let mut service = crust::Service::<UniqueId>::with_config(event_sender, config, rand::random()).unwrap();

        service.start_listening_tcp().unwrap();
        service.start_service_discovery();
        let service = Arc::new(Mutex::new(service));

        NetworkService {
            channel_rx,
            category_rx,
            service
        }
    }

    pub fn start(&mut self) {
        
        // let network

        // let service = mut self.service.clone();
        // maidsafe_utilities::thread::named("NetworkService event handler", move || {
        // }); 
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}